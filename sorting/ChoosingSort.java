/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sorting;

import java.util.Comparator;
import java.util.List;


public class ChoosingSort extends Sort {

    @Override
    public <T> void sort(List<T> list, Comparator<T> comparator) {
        
        
        T car;
        int j = 0;
        for (int i = 1; i < list.size(); i++) {
            car = list.get(i);
            j = i - 1;
            
            while (j > -1 && comparator.compare(car, list.get(j)) < 0) {
                //System.out.println("Before: " + list.get(j+1));
                list.set(j+1, list.get(j));
                //System.out.println("After: " + list.get(j+1));
                j--;
            }
            //tab[j+1] = key;
            list.set(j+1, car);
        }
    }

}
