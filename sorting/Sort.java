/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorting;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Kacper
 */
public abstract class Sort {

    public <T extends Comparable<T>> void sort(List<T> list) {
        sort(list, new Comparator<T>() {

            @Override
            public int compare(T t, T t1) {
                return t.compareTo(t1);
            }
        });
    }
    
    public abstract <T> void sort(List<T> list, Comparator<T> comparator);
}
