/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sorting;

import java.util.Comparator;
import java.util.List;


public class QuickSort extends Sort{
    

    @Override
    public <T> void sort(List<T> list, Comparator<T> comparator) {
        quickSort(list, comparator, 0, list.size() - 1);
    }
    
    private <T> void quickSort(List<T> list, Comparator<T> comparator, int p, int r) {
        if (p <= r) {
            //recursive calling of the quicksort() method
            //q is the index of the pivot element
            //method split() reorganizes the list in the following order:
            //list: [smallerElements, pivot, biggerElements]
            int q = split(list, comparator, p, r);
            //do the same with the smallerElements list
            quickSort(list, comparator, p, q-1);
            //do the same with the biggerElements list
            quickSort(list, comparator, q+1, r);
        }
    }
    
    private <T> int split(
            List<T> list,
            Comparator<T> comparator,
            int firstElementIndex,
            int pivotIndex) {
        //q is the index of the element which is being replaced
        int q = firstElementIndex;
        
        for (int u = firstElementIndex; u < pivotIndex; u++) {
            if(comparator.compare(list.get(u), list.get(pivotIndex)) <= 0) {
                T buf;
                //replacing element 'u' with element 'pivotIndex' 
                buf = list.get(u);
                list.set(u, list.get(q));
                list.set(q, buf);
                q++;                
            }
        }
        //put the pivot element between the sets 
        //containing elements smaller and bigger than this element
        //q is the new index of the pivot
        T buf = list.get(pivotIndex);
        list.set(pivotIndex, list.get(q));
        list.set(q, buf);
        return q;
    }

}
