/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sort.mainClasses;


public class Vehicle {
    
    public Vehicle() {}
    
    public Vehicle(String brand, String model, int productionYear) {
        this.aBrand = brand;
        this.aModel = model;
        this.aProductionYear = productionYear;
    }
    String aBrand, aModel;
    int aProductionYear;
    
    public String getBrand() {
        return aBrand;
    }
    
    public String getModel() {
        return aModel;
    }
    
    public int getProductionYear() {
        return aProductionYear;
    }

    @Override
    public String toString() {
        return "Vehicle{" + "Brand=" + aBrand + ", Model=" + aModel
                + ", ProductionYear=" + aProductionYear + '}';
    }
    
    

}
