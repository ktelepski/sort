package sort.mainClasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import sorting.ChoosingSort;
import sorting.QuickSort;

public class SortLauncher {

    public static void main(String[] args) {
        ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>(32);
        
        Vehicle peugeot = new Vehicle("Peugeot", "206", 2000);
        vehicleList.add(peugeot);
        Vehicle mazda = new Vehicle("Mazda", "3", 2008);
        vehicleList.add(mazda);
        Vehicle nissan = new Vehicle("Nissan", "350z", 2004);
        vehicleList.add(nissan);
        Vehicle audi = new Vehicle("Audi", "A3", 2010);
        vehicleList.add(audi);
        
        System.out.println("\nVehicles before sorting:");
        for (Vehicle element : vehicleList) {
            System.out.println(element);
        }
        
        sorting.Sort quickSort = new QuickSort();
        quickSort.sort(vehicleList, new Comparator<Vehicle>() {

            @Override
            public int compare(Vehicle t, Vehicle t1) {
                return t.getBrand().compareTo(t1.getBrand());
            }
        });
        
        System.out.println("\nVehicles sorted: ");
        for (Vehicle element : vehicleList) {
            System.out.println(element);
        }
        
    }
    
}
